"""
Hangman.py
Plays the classic word-guessing game
Author: Richard Harrington
Last updated: 9/13/2013
"""

import math
import random

def build_hangman():
    # Build a list of the hangman pieces
    drawing=[]
    drawing.append("\n\n\n\n\n\n______")
    drawing.append("\n\n\n\n\n\n__|___")
    drawing.append("\n\n\n\n\n  |\n__|___")
    drawing.append("\n\n\n\n  |\n  |\n__|___")
    drawing.append("\n\n\n  |\n  |\n  |\n__|___")
    drawing.append("\n\n  |\n  |\n  |\n  |\n__|___")
    drawing.append("\n   _\n  | |\n  |\n  |\n  |\n__|___")
    drawing.append("\n   _\n  | |\n  | o\n  |\n  |\n__|___")
    drawing.append("\n   _\n  | |\n  | o\n  | |\n  |\n__|___")
    drawing.append("\n   _\n  | |\n  | o\n  |-|-\n  |\n__|___")
    drawing.append("\n   _\n  | |\n  | o\n  |-|-\n  |/ \\\n__|___")
    return drawing

def draw_puzzle(word, revealed):
    # Draw the blank spaces and the revealed spaces
    printline=""
    for c in word:
        if c in revealed:
            printline+=c+" "
        else:
            printline+="_ "
    print(printline)

def pick_word(dictionary):
    # Select a random word from the dictionary
    with open(dictionary) as f:
        words=f.read().splitlines()
    f.close()

    i=int(math.floor((random.random()*len(words))))
    return words[i]

def count_letters(word):
    # Count the different letters in a word
    letters=[]
    for c in word:
        if c not in letters:
            letters.append(c)
    return len(letters)

def get_dictionary():
    d=input("Would you like to play an easy (E), medium (M), or hard (H) game?")
    if d.lower()=='e':
        return 'easy.txt'
    elif d.lower()=='m':
        return 'medium.txt'
    elif d.lower()=='h':
        return 'hard.txt'
    
def play_game():
    # Setup and play the game
    my_hangman=build_hangman()
    wrong_guesses=0
    turn=1
    word=pick_word(get_dictionary())
    letter_count=count_letters(word)
    revealed=[]
    letter=""
    guessed=[]

    # Play the game
    while wrong_guesses < len(my_hangman) and letter_count > len(revealed):

        
        # Draw the board
        printline=""
        print(my_hangman[wrong_guesses],"\n")

        # Show the already-guessed letters
        for a in sorted(guessed):
            printline+=a+" "
        print(printline)
        print("\nTurn: ",str(turn))
        draw_puzzle(word,revealed)

        # Get a letter, check to see if it's in the word
        letter=input("Please enter one letter:\n")

        if letter not in guessed:
            if letter in word:
                revealed.append(letter)
            else:
                wrong_guesses+=1
            guessed.append(letter)
        turn+=1

    # Show the word after the game ends
    draw_puzzle(word,word)

    if wrong_guesses<len(my_hangman) :
       print("You won!")
    else:
       print("Try again next time!")


play_game()
